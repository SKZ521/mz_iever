//
//  IETimeView.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IETimeView : UIView

- (void)setTimeWithString:(NSString *)time;
@end
