//
//  I_EverAppDelegate.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-27.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface I_EverAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
