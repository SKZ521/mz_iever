//
//  main.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-27.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "I_EverAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([I_EverAppDelegate class]));
    }
}
