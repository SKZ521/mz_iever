//
//  MainListViewController.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-27.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "I_EverBaseViewController.h"
#import "IETimeView.h"

@interface MainListViewController : I_EverBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *toolBar;
@property (weak, nonatomic) IBOutlet UIButton *todayButton;
@property (weak, nonatomic) IBOutlet IETimeView *timeView;

- (IBAction)gobackToday:(id)sender;

- (IBAction)optionSelected:(id)sender;
@end
