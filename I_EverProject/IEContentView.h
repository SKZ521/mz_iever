//
//  IEContentView.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, IEModuleType)
{
    IETypeVideo = 1800,    // 视频
    IETypeMerch,           // 商品
    IETypeToilette         // 美妆
};

@protocol IEContentViewDelegate <NSObject>

@optional
- (void)videoPlaySelectedWithUrl:(NSString *)url;

@end

@interface IEContentView : UIView

@property(nonatomic, assign)IEModuleType moduleType;
@property(nonatomic, strong)id dataModel;
@property(nonatomic, assign)id<IEContentViewDelegate> delegate;

@end
