//
//  IECommonMethod.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IECommonMethod.h"

@implementation IECommonMethod

+ (NSString *)timeStringFromDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter stringFromDate:date];
}

+ (NSDate *)dateFromTimeString:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    return [formatter dateFromString:string];
}

+ (NSString *)weekFromDate:(NSDate *)date
{
    return nil;
}

@end
