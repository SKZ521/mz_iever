//
//  MainListViewController.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-27.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "MainListViewController.h"
#import "IEContentView.h"

#import "IEVideoListRequest.h"
#import "IEMerchListRequest.h"
#import "IENewsListRequest.h"
#import "IELikeVideoRequest.h"
#import "IELikeMerchRequest.h"

#import "IEVideoModel.h"

#import "NSDate+Extension.h"

#import <MediaPlayer/MediaPlayer.h>

#define kVideoButtonTag 1800
#define kMerchButtonTag 1801
#define kNewsButtonTag  1802

@interface MainListViewController ()<UITableViewDataSource,UITableViewDelegate,IEContentViewDelegate>
{
    MPMoviePlayerViewController *movie;
}
// data container
@property(nonatomic, strong)NSMutableArray *dataArray;

@property(nonatomic, assign)IEModuleType moduleType;

@end

@implementation MainListViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // init container
        self.dataArray = [NSMutableArray array];
        
        self.moduleType = IETypeVideo;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // hide navigation bar
    self.navigationController.navigationBarHidden = YES;
    // we need a landscape table view
    self.tableView.transform = CGAffineTransformMakeRotation(3*M_PI/2);
    
    // request data
    [self startRequestWithDateString:@""];
}

#pragma mark - private method
- (void)requestVideoList
{
    [IEVideoListRequest requestWithParameters:@{@"dateString":@"2014-06-07"} indicatorParentView:self.view onRequestStart:^(BaseDataCenter *request) {
        
    } onRequestFinished:^(BaseDataCenter *request) {
        self.dataArray = request.result.resultObject;
//        [_dataArray addObjectsFromArray:request.result.resultObject];
        [_tableView reloadData];
    } onRequestCanceled:^(BaseDataCenter *request) {
        
    } onRequestFailed:^(BaseDataCenter *request, NSError *error) {
        
    }];
}

- (void)requestMerchList
{
    [IEMerchListRequest requestWithParameters:@{@"dateString":@"2014-06-14"} indicatorParentView:self.view onRequestStart:^(BaseDataCenter *request) {
        
    } onRequestFinished:^(BaseDataCenter *request) {
        self.dataArray = request.result.resultObject;
        //        [_dataArray addObjectsFromArray:request.result.resultObject];
        [_tableView reloadData];
    } onRequestCanceled:^(BaseDataCenter *request) {
        
    } onRequestFailed:^(BaseDataCenter *request, NSError *error) {
        
    }];
}

- (void)requestNewsList
{
    [IENewsListRequest requestWithParameters:@{@"dateString":@"2014-05-11"} indicatorParentView:self.view onRequestStart:^(BaseDataCenter *request) {
        
    } onRequestFinished:^(BaseDataCenter *request) {
        self.dataArray = request.result.resultObject;
        //        [_dataArray addObjectsFromArray:request.result.resultObject];
        [_tableView reloadData];
    } onRequestCanceled:^(BaseDataCenter *request) {
        
    } onRequestFailed:^(BaseDataCenter *request, NSError *error) {
        
    }];
}

- (void)startRequestWithDateString:(NSString *)aDStr
{
    switch (_moduleType)
    {
        case IETypeVideo:// 视频
            [self requestVideoList];
            break;
        case IETypeMerch:// 商品
            [self requestMerchList];
            break;
        case IETypeToilette:// 新闻
            [self requestNewsList];
            break;
        default:
            break;
    }
}

/**
 *  show today
 *
 *  @param sender
 */
- (IBAction)gobackToday:(id)sender
{
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (IBAction)optionSelected:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button.tag != _moduleType)
    {
        _moduleType = button.tag;
        [self startRequestWithDateString:@""];
    }
}

//- (UIColor *)
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDelegate & UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
    cell.transform = CGAffineTransformMakeRotation(M_PI/2);
    
    UIView *layerView = (UIView *)[cell.contentView viewWithTag:1000];
    layerView.clipsToBounds = YES;
    layerView.layer.cornerRadius = 5.0f;
    
    NSInteger index = [indexPath row];
    IEContentView *cView = [[IEContentView alloc] initWithFrame:layerView.bounds];
    cView.delegate = self;
    cView.moduleType = _moduleType;
    cView.dataModel = _dataArray[index];
    [layerView addSubview:cView];
    
    return cell;
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
    
    CGFloat offset = scrollView.contentOffset.y;
    NSInteger index = offset/320;
    NSLog(@"offset:%f,index:%d",offset,index);

    if (_toolBar.hidden)
    {
        _toolBar.hidden = NO;
    }
    
    _todayButton.hidden = !offset;
    
    [_timeView setTimeWithString:@"2014-07-08"];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
    
    if (!_toolBar.hidden)
    {
        _toolBar.hidden = YES;
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
    
    _todayButton.hidden = !scrollView.contentOffset.y;
}

#pragma mark - IEContentViewDelegate
- (void)videoPlaySelectedWithUrl:(NSString *)url
{
    NSLog(@"play url:%@",url);
//    url = @"http://devimages.apple.com/iphone/samples/bipbop/gear1/prog_index.m3u8";
//    url = @"http://player.youku.com/player.php/sid/XNDUwNjc4MzA4/v.swf";
//    url = @"http://www.iqiyi.com/common/openiframe.html?tvid=223033100&videoid=868dea90087505a9c21675b2804ce154&albumid=223033100&flashvars=bd%3D1%26coop%3dcoop_1";
    NSURL *cUrl = [NSURL URLWithString:url];
    movie = [[MPMoviePlayerViewController alloc]initWithContentURL:cUrl];
    movie.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
    [movie.moviePlayer prepareToPlay];
    [self presentMoviePlayerViewControllerAnimated:movie];
    [movie.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
    
    [movie.view setBackgroundColor:[UIColor clearColor]];
    
    [movie.view setFrame:self.view.bounds];
//    [movie.moviePlayer play];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                           selector:@selector(movieFinishedCallback:)
                                               name:MPMoviePlayerPlaybackDidFinishNotification
                                             object:movie.moviePlayer];
}

- (void)movieFinishedCallback:(NSNotification*)notify
{
    // 视频播放完或者在presentMoviePlayerViewControllerAnimated下的Done按钮被点击响应的通知。
    
    MPMoviePlayerController* theMovie = [notify object];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self
     
                                                  name:MPMoviePlayerPlaybackDidFinishNotification
     
                                                object:theMovie];
    
    [self dismissMoviePlayerViewControllerAnimated];
    
}

@end
