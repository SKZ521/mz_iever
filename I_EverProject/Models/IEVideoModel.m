//
//  IEVideoModel.m
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IEVideoModel.h"

@implementation IEVideoModel

- (id)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init])
    {
        self.videoId = dict[@"videoId"];
        self.name = dict[@"name"];
        self.aiqiyiUrl = dict[@"aiqiyiUrl"];
        self.bigPic = dict[@"bigPic"];
        self.remark = dict[@"remark"];
        self.playCount = dict[@"playCount"];
        self.updateDate = dict[@"updateDate"];
        self.likecountapp = dict[@"likecountapp"];
    }
    return self;
}

@end
