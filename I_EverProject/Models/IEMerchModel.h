//
//  IEMerchModel.h
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IEMerchModel : NSObject
@property(nonatomic, copy)NSString *productId;
@property(nonatomic, copy)NSString *name;
@property(nonatomic, copy)NSString *remark;//描述
@property(nonatomic, copy)NSString *productimg;//": "/uploads/img/2aeae86c768898af09364aa794e84532.png",
@property(nonatomic, copy)NSString *littleimg;//": "/uploads/img/ffb52e9ce2efbd2a5c4858a45e711271.png",
@property(nonatomic, copy)NSString *bigimg;//": "/uploads/img/ffb52e9ce2efbd2a5c4858a45e711271.png",
@property(nonatomic, copy)NSString *updateDate;
@property(nonatomic, copy)NSString *likecountapp;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
