//
//  IENewsModel.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IENewsModel.h"

@implementation IENewsModel

- (id)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init])
    {
        self.newsId = dict[@"newsId"];
        self.title = dict[@"title"];
        self.memo = dict[@"memo"];
        self.showimg = dict[@"showimg"];
        self.updateDate = dict[@"updateDate"];
    }
    return self;
}

@end
