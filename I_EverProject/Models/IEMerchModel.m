//
//  IEMerchModel.m
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IEMerchModel.h"

@implementation IEMerchModel

- (id)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init])
    {
        self.productId = dict[@"productId"];
        self.name = dict[@"name"];
        self.remark = dict[@"remark"];
        self.productimg = dict[@"productimg"];
        self.littleimg = dict[@"littleimg"];
        self.bigimg = dict[@"bigimg"];
        self.updateDate = dict[@"updateDate"];
        self.likecountapp = dict[@"likecountapp"];
    }
    return self;
}

@end
