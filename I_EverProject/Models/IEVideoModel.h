//
//  IEVideoModel.h
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IEVideoModel : NSObject
@property(nonatomic, copy)NSString *videoId;
@property(nonatomic, copy)NSString *name;
@property(nonatomic, copy)NSString *mp4Url;// 临时不用
@property(nonatomic, copy)NSString *aiqiyiUrl;// 播放地址
@property(nonatomic, copy)NSString *bigPic;
@property(nonatomic, copy)NSString *remark;// 内容描述
@property(nonatomic, copy)NSString *playCount;
@property(nonatomic, copy)NSString *updateDate;
@property(nonatomic, copy)NSString *likecountapp;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
