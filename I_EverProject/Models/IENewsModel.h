//
//  IENewsModel.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IENewsModel : NSObject
@property(nonatomic, copy)NSString *newsId;
@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *memo; // 描述
@property(nonatomic, copy)NSString *showimg;
@property(nonatomic, copy)NSString *updateDate;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
