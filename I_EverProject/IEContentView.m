//
//  IEContentView.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IEContentView.h"

#import "IEVideoModel.h"
#import "IEMerchModel.h"
#import "IENewsModel.h"

#import "UIImageView+WebCache.h"

@interface IEContentView ()<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, strong)UITableView *contentTableView;

@end

@implementation IEContentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _contentTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        _contentTableView.allowsSelection = NO;
        _contentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _contentTableView.tableFooterView = [UIView new];
        _contentTableView.delegate = self;
        _contentTableView.dataSource = self;
        [self addSubview:_contentTableView];
    }
    return self;
}

#pragma mark - private method
/**
 @method 获取指定宽度情况ixa，字符串value的高度
 @param value 待计算的字符串
 @param fontSize 字体的大小
 @param andWidth 限制字符串显示区域的宽度
 @result float 返回的高度
 */
- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    CGSize sizeToFit = [value sizeWithFont:[UIFont systemFontOfSize:fontSize] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];//此处的换行类型（lineBreakMode）可根据自己的实际情况进行设置
    return sizeToFit.height;
}

- (void)playClicked:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(videoPlaySelectedWithUrl:)])
    {
        IEVideoModel *model = (IEVideoModel *)_dataModel;
        [_delegate videoPlaySelectedWithUrl:model.aiqiyiUrl];
    }
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_moduleType == IETypeVideo)
    {
        IEVideoModel *model = (IEVideoModel *)_dataModel;
        
        static NSString *identifier = @"VideoContentCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            // 图片（包括视频）
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 310.0f, 155.0f)];
            UIImage *placeHolder = [UIImage imageNamed:@"Placeholder_goods"];
            NSString *imgPath = model.bigPic;
            if (![imgPath hasPrefix:@"http://"])
            {
                imgPath = [NSString stringWithFormat:@"%@%@",kDomain, imgPath];
            }
            [imageView sd_setImageWithURL:[NSURL URLWithString:imgPath] placeholderImage:placeHolder];
            imageView.tag = 100;
            [cell.contentView addSubview:imageView];
            imageView.hidden = YES;
            
            UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [playButton setFrame:CGRectMake(260.0, 100.0f, 50.0f, 50.0f)];
            [playButton setBackgroundColor:[UIColor purpleColor]];
            playButton.tag = 101;
            [playButton addTarget:self action:@selector(playClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:playButton];
            playButton.hidden = YES;
        }
        NSInteger index = [indexPath row];
        UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:100];
        UIButton *play = (UIButton *)[cell.contentView viewWithTag:101];
        
        imgView.hidden = YES;
        play.hidden = YES;
        switch (index)
        {
            case 0:
                cell.textLabel.text = model.name;
                break;
            case 1:
            {
                imgView.hidden = NO;
                play.hidden = NO;
            }
                break;
            case 2:
                cell.textLabel.text = model.remark;
                break;
            default:
                break;
        }
        return cell;
    } else if (_moduleType == IETypeMerch) {
        IEMerchModel *model = (IEMerchModel *)_dataModel;
        
        static NSString *identifier = @"MerchContentCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            // 图片（包括视频）
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 310.0f, 155.0f)];
            UIImage *placeHolder = [UIImage imageNamed:@"Placeholder_goods"];
            NSString *imgPath = model.productimg;
            if (![imgPath hasPrefix:@"http://"])
            {
                imgPath = [NSString stringWithFormat:@"%@%@",kDomain, imgPath];
            }
            [imageView sd_setImageWithURL:[NSURL URLWithString:imgPath] placeholderImage:placeHolder];
            imageView.tag = 100;
            [cell.contentView addSubview:imageView];
            imageView.hidden = YES;
        }
        NSInteger index = [indexPath row];
        switch (index)
        {
            case 0:
                cell.textLabel.text = model.name;
                break;
            case 1:
            {
                UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:100];
                imgView.hidden = NO;
            }
                break;
            case 2:
                cell.textLabel.text = model.remark;
                break;
            default:
                break;
        }
        return cell;
    } else {
        IENewsModel *model = (IENewsModel *)_dataModel;
        
        static NSString *identifier = @"NewsContentCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            // 图片（包括视频）
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 310.0f, 155.0f)];
            UIImage *placeHolder = [UIImage imageNamed:@"Placeholder_goods"];
            NSString *imgPath = model.showimg;
            if (![imgPath hasPrefix:@"http://"])
            {
                imgPath = [NSString stringWithFormat:@"%@%@",kDomain, imgPath];
            }
            [imageView sd_setImageWithURL:[NSURL URLWithString:imgPath] placeholderImage:placeHolder];
            imageView.tag = 100;
            [cell.contentView addSubview:imageView];
            imageView.hidden = YES;
        }
        NSInteger index = [indexPath row];
        switch (index)
        {
            case 0:
                cell.textLabel.text = model.title;
                break;
            case 1:
            {
                UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:100];
                imgView.hidden = NO;
            }
                break;
            case 2:
                cell.textLabel.text = model.memo;
                break;
            default:
                break;
        }
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = [indexPath row];
    CGFloat rowHeight = .0f;
    switch (index)
    {
        case 0:
            rowHeight = 50.0f;
            break;
        case 1:
            rowHeight = 155.0f;
            break;
        case 2:
            rowHeight = 44.0f;
            break;
        default:
            break;
    }
    return rowHeight;
}

@end
