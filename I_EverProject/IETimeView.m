//
//  IETimeView.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IETimeView.h"
#import "IECommonMethod.h"
#import "NSDate+Extension.h"

@interface IETimeView ()

@property(nonatomic, strong)UILabel *dayLabel;
@property(nonatomic, strong)UILabel *monthLabel;
@property(nonatomic, strong)UILabel *weekLabel;

@end


@implementation IETimeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self createSubViews];
    }
    return self;
}

- (void)awakeFromNib
{
    [self createSubViews];
}

// 此处需后期优化，使用constraint是最佳选择
- (void)createSubViews
{
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width/2, height)];
    [_dayLabel setFont:[UIFont systemFontOfSize:30.0f]];
    [_dayLabel setTextAlignment:NSTextAlignmentRight];
    [_dayLabel setTextColor:[UIColor whiteColor]];
    
    _monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/2, 0.0f, width/2, height/2)];
    [_monthLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [_monthLabel setTextColor:[UIColor whiteColor]];
    
    _weekLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/2, height/2, width/2, height/2)];
    [_weekLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [_weekLabel setTextColor:[UIColor whiteColor]];
    
    [self addSubview:_dayLabel];
    [self addSubview:_monthLabel];
    [self addSubview:_weekLabel];
}

- (void)setTimeWithString:(NSString *)time
{
    NSDate *date = [IECommonMethod dateFromTimeString:time];
    
    [_monthLabel setText:[NSString stringWithFormat:@"%d月",[date month]]];
    [_weekLabel setText:[self getWeekWithCode:[date week]]];
    [_dayLabel setText:[NSString stringWithFormat:@"%d",[date day]]];
}

- (NSString *)getWeekWithCode:(NSInteger)code
{
    if (code<1 || code>7)
    {
        return nil;
    }
    
    NSString *week = nil;
    switch (code)
    {
        case 1:
            week = @"星期一";
            break;
        case 2:
            week = @"星期二";
            break;
        case 3:
            week = @"星期三";
            break;
        case 4:
            week = @"星期四";
            break;
        case 5:
            week = @"星期五";
            break;
        case 6:
            week = @"星期六";
            break;
        case 7:
            week = @"星期日";
            break;
        default:
            break;
    }
    return week;
}

@end
