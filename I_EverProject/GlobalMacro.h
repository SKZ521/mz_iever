//
//  GlobalMacro.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-27.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#ifndef I_EverProject_GlobalMacro_h
#define I_EverProject_GlobalMacro_h

// NSLog
#ifndef __OPTIMIZE__
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...) {}
#endif

// singleton
#define LLBTOBJECT_SINGLETON_BOILERPLATE(_object_name_, _shared_obj_name_) \
static _object_name_ *z##_shared_obj_name_ = nil;  \
+ (_object_name_ *)_shared_obj_name_ {             \
@synchronized(self) {                            \
if (z##_shared_obj_name_ == nil) {             \
/* Note that 'self' may not be the same as _object_name_ */                               \
/* first assignment done in allocWithZone but we must reassign in case init fails */      \
z##_shared_obj_name_ = [[self alloc] init];                                               \
}                                              \
}                                                \
return z##_shared_obj_name_;                     \
}                                                  \
+ (id)allocWithZone:(NSZone *)zone {               \
@synchronized(self) {                            \
if (z##_shared_obj_name_ == nil) {             \
z##_shared_obj_name_ = [super allocWithZone:zone]; \
return z##_shared_obj_name_;                 \
}                                              \
}                                                \
\
/* We can't return the shared instance, because it's been init'd */     \
return nil;                                    \
}                                                  \
- (id)retain {                                     \
return self;                                   \
}                                                  \
- (NSUInteger)retainCount {                        \
return NSUIntegerMax;                          \
}                                                  \
- (oneway void)release {                                  \
}                                                  \
- (id)autorelease {                                \
return self;                                   \
}                                                  \
- (id)copyWithZone:(NSZone *)zone {                \
return self;                                   \
}                                                  \

#endif


// main color
#define kMainColor [UIColor colorWithRed:243.0f/255.0f green:68.0f/255.0f blue:34.0f/255.0f alpha:1.0f]


#define kDomain @"http://218.244.148.190"
