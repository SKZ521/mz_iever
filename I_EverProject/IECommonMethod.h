//
//  IECommonMethod.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-6-30.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IECommonMethod : NSObject

/**
 *  将date转换成字符串
 *
 *  @param date
 *
 *  @return 返回格式如：2014-07-05
 */
+ (NSString *)timeStringFromDate:(NSDate *)date;

/**
 *  将字符串转换成date
 *
 *  @param string
 *
 *  @return
 */
+ (NSDate *)dateFromTimeString:(NSString *)string;

/**
 *  获取指定时间是周几
 *
 *  @param date
 *
 *  @return 如：星期二
 */
+ (NSString *)weekFromDate:(NSDate *)date;

@end
