//
//  RequestManager.m
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import "RequestManager.h"

@interface RequestManager ()

@property(retain, nonatomic)NSMutableArray *requests;

@end

@implementation RequestManager

LLBTOBJECT_SINGLETON_BOILERPLATE(RequestManager, sharedManager)

- (id)init
{
    if (self = [super init])
    {
        self.requests = [NSMutableArray array];
    }
    return self;
}

- (void)dealloc
{
    self.requests = nil;
    
    [super dealloc];
}

#pragma mark - public methods

- (void)addRequest:(BaseDataCenter *)request
{
    [_requests addObject:request];
}

- (void)removeRequest:(BaseDataCenter *)request
{
    [_requests removeObject:request];
}

@end
