//
//  IENewsListRequest.m
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-7-1.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IENewsListRequest.h"
#import "IENewsModel.h"

@implementation IENewsListRequest

- (RequestMethod)getRequestMethod
{
    return kRequestMethodPost;
}

- (NSString *)getRequestUrl
{
    return [NSString stringWithFormat:@"http://218.244.148.190/api/News.asmx/getNewsList"];
}

- (void)processResult
{
    [super processResult];
    NSLog(@"%s--获取新闻列表返回信息-----%@",__FUNCTION__,self.responseObject);
    if ([self.responseObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *videos = [NSMutableArray array];
        for (NSDictionary *dict in self.responseObject)
        {
            IENewsModel *model = [[IENewsModel alloc] initWithDictionary:dict];
            [videos addObject:model];
        }
        self.result.resultObject = videos;
    } else {
        self.result.code = -1;
        self.result.message = @"数据格式错误";
    }
}

@end
