//
//  ResponseResult.m
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import "ResponseResult.h"

@implementation ResponseResult
- (id)initWithResult:(id)resultObject code:(id)code message:(NSString *)msg
{
    if (self = [super init])
    {
        if ([resultObject isKindOfClass:[NSArray class]])
        {
            self.resultArray = resultObject;
        } else {
            self.resultObject = resultObject;
        }
        _code = [code integerValue];
        if (_code == 0)
        {
            _isSuccess = YES;
        }
        _message = msg;
    }
    return self;
}

- (void)dealloc
{
    [_message release];
    [_resultObject release];
    [_resultArray release];
    [super dealloc];
}

@end
