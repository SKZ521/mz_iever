//
//  IEMerchListRequest.m
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IEMerchListRequest.h"
#import "IEMerchModel.h"

@implementation IEMerchListRequest

- (RequestMethod)getRequestMethod
{
    return kRequestMethodPost;
}

- (NSString *)getRequestUrl
{
    return [NSString stringWithFormat:@"http://218.244.148.190/api/Product.asmx/getProductList"];
}

- (void)processResult
{
    [super processResult];
    NSLog(@"%s---商品列表返回信息----%@",__FUNCTION__,self.responseObject);
    if ([self.responseObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *videos = [NSMutableArray array];
        for (NSDictionary *dict in self.responseObject)
        {
            IEMerchModel *model = [[IEMerchModel alloc] initWithDictionary:dict];
            [videos addObject:model];
        }
        self.result.resultObject = videos;
    } else {
        self.result.code = -1;
        self.result.message = @"数据格式错误";
    }
}

@end
