//
//  IELikeVideoRequest.m
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IELikeVideoRequest.h"

@implementation IELikeVideoRequest

- (RequestMethod)getRequestMethod
{
    return kRequestMethodPost;
}

- (NSString *)getRequestUrl
{
    return [NSString stringWithFormat:@"http://218.244.148.190:80/api/Video.asmx/likeVideo"];
}

- (void)processResult
{
    [super processResult];
    NSLog(@"%s--视频点赞返回信息-----%@",__FUNCTION__,self.responseObject);
}

@end
