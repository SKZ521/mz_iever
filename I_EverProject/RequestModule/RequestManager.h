//
//  RequestManager.h
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseDataCenter;

@interface RequestManager : NSObject

+ (RequestManager *)sharedManager;

- (void)addRequest:(BaseDataCenter*)request;
- (void)removeRequest:(BaseDataCenter*)request;

@end
