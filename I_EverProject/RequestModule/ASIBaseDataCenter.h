//
//  ASIBaseDataCenter.h
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface ASIBaseDataCenter : NSObject

@property(retain, nonatomic)ASIFormDataRequest *request;

- (void)startGetRequestWithUrl:(NSString *)url parameters:(NSDictionary *)parameters;

- (void)startPostRequestWithUrl:(NSString *)url parameters:(NSDictionary *)parameters;

@end