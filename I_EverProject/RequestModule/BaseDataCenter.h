//
//  BaseDataCenter.h
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import "ASIBaseDataCenter.h"
#import "DataCacheManager.h"
#import "ResponseResult.h"

typedef NS_ENUM(NSUInteger, RequestMethod)
{
    kRequestMethodGet = 0,
	kRequestMethodPost = 1,           // content type = @"application/x-www-form-urlencoded"
	kRequestMethodMultipartPost = 2   // content type = @"multipart/form-data"
};

@protocol DataRequestDelegate;
@interface BaseDataCenter : ASIBaseDataCenter

@property(assign, nonatomic)id<DataRequestDelegate> delegate;

@property (copy, nonatomic)void (^onRequestStartBlock)(BaseDataCenter *);
@property (copy, nonatomic)void (^onRequestFinishBlock)(BaseDataCenter *);
@property (copy, nonatomic)void (^onRequestCanceled)(BaseDataCenter *);
@property (copy, nonatomic)void (^onRequestFailedBlock)(BaseDataCenter *,NSError *);
// 请求返回结果
@property (retain, nonatomic)ResponseResult *result;
@property (retain, nonatomic)id responseObject;

#pragma mark - init methods using delegate
+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate;

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
           withParameters:(NSDictionary*)params;

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
               parameters:(NSDictionary*)params
                 cacheKey:(NSString*)cache
                cacheType:(DataCacheManagerCacheType)cacheType;

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
      indicatorParentView:(UIView*)indiView;

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
               parameters:(NSDictionary*)params
      indicatorParentView:(UIView*)indiView;

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
               parameters:(NSDictionary*)params
      indicatorParentView:(UIView*)indiView
                 cacheKey:(NSString*)cache
                cacheType:(DataCacheManagerCacheType)cacheType;

- (id)initWithDelegate:(id<DataRequestDelegate>)delegate
            parameters:(NSDictionary*)params
   indicatorParentView:(UIView*)indiView
              cacheKey:(NSString*)cacheKey
             cacheType:(DataCacheManagerCacheType)cacheType;

- (id)initWithDelegate:(id<DataRequestDelegate>)delegate
            requestUrl:(NSString*)url
            parameters:(NSDictionary*)params
   indicatorParentView:(UIView*)indiView
        loadingMessage:(NSString*)msg
              cacheKey:(NSString*)cache
             cacheType:(DataCacheManagerCacheType)cacheType;

#pragma mark - init methods using blocks
+ (id)requestWithParameters:(NSDictionary*)params
        indicatorParentView:(UIView*)indiView
          onRequestFinished:(void(^)(BaseDataCenter *request))onFinishedBlock;

+ (id)requestWithParameters:(NSDictionary*)params
        indicatorParentView:(UIView*)indiView
             onRequestStart:(void(^)(BaseDataCenter *request))onStartBlock
          onRequestFinished:(void(^)(BaseDataCenter *request))onFinishedBlock
          onRequestCanceled:(void(^)(BaseDataCenter *request))onCanceledBlock
            onRequestFailed:(void(^)(BaseDataCenter *request, NSError *error))onFailedBlock;

- (id)initWithParameters:(NSDictionary *)params
              requestUrl:(NSString *)url
     indicatorParentView:(UIView *)indiView
          loadingMessage:(NSString *)msg
                cacheKey:(NSString *)cache
               cacheType:(DataCacheManagerCacheType)cacheType
          onRequestStart:(void(^)(BaseDataCenter *request))onStartBlock
       onRequestFinished:(void(^)(BaseDataCenter *request))onFinishedBlock
       onRequestCanceled:(void(^)(BaseDataCenter *request))onCanceledBlock
         onRequestFailed:(void(^)(BaseDataCenter *request, NSError *error))onFailedBlock;

/**
 *  waitting view show/hide
 */
- (void)showWaitView;
- (void)hideWaitView;

/**
 *  子类根据需求选择性的重写
 */
// 数据解析完成后调用
- (void)processResult;
// 请求方式 GET/POST
- (RequestMethod)getRequestMethod;
// 网络地址
- (NSString*)getRequestUrl;

- (void)cancelRequest;

/**
 *  通过code检查error的名字
 *
 *  @param code 错误码
 *
 *  @return 错误名
 */
- (NSString *)getErrorNameWithErrorCode:(NSInteger)code;

@end


#pragma mark - DataRequestDelegate

@protocol DataRequestDelegate <NSObject>

@optional

- (void)requestDidStartLoad:(BaseDataCenter*)request;
- (void)requestDidFinishLoad:(BaseDataCenter*)request;
- (void)requestDidCancelLoad:(BaseDataCenter*)request;
- (void)request:(BaseDataCenter*)request didFailLoadWithError:(NSError*)error;

@end