//
//  IEVideoListRequest.m
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IEVideoListRequest.h"
#import "IEVideoModel.h"

@implementation IEVideoListRequest

- (RequestMethod)getRequestMethod
{
    return kRequestMethodPost;
}

- (NSString *)getRequestUrl
{
    return [NSString stringWithFormat:@"http://218.244.148.190/api/Video.asmx/getVideoList"];
}

- (void)processResult
{
    [super processResult];
    NSLog(@"%s--视频列表返回信息----%@",__FUNCTION__,self.responseObject);
    if ([self.responseObject isKindOfClass:[NSArray class]])
    {
        NSMutableArray *videos = [NSMutableArray array];
        for (NSDictionary *dict in self.responseObject)
        {
            IEVideoModel *model = [[IEVideoModel alloc] initWithDictionary:dict];
            [videos addObject:model];
        }
        self.result.resultObject = videos;
    } else {
        self.result.code = -1;
        self.result.message = @"数据格式错误";
    }
}

@end
