//
//  IEVideoListRequest.h
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "BaseDataCenter.h"

// 获取视频列表
@interface IEVideoListRequest : BaseDataCenter

@end
