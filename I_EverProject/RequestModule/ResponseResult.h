//
//  ResponseResult.h
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResponseResult : NSObject
@property (assign, nonatomic) BOOL isSuccess;
@property (assign, nonatomic) NSInteger code;
@property (copy  , nonatomic) NSString  *message;
@property (retain, nonatomic) id        resultObject;
@property (retain, nonatomic) NSArray   *resultArray;

- (id)initWithResult:(id)resultObject code:(id)code message:(NSString *)msg;

@end
