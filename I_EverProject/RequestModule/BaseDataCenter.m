//
//  BaseDataCenter.m
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import "BaseDataCenter.h"
#import "RequestManager.h"
#import "MBProgressHUD.h"
#import "JSONKit.h"

@interface BaseDataCenter ()
// 请求地址
@property (copy, nonatomic)NSString *requestUrl;
// 等待框父视图
@property (retain, nonatomic)UIView *indicatorView;
// 等待提示
@property (copy, nonatomic)NSString *loadingMessage;

@property (assign, nonatomic)DataCacheManagerCacheType cacheType;
@property (copy, nonatomic)NSString *cacheKey;

@end

@implementation BaseDataCenter
#pragma mark - init methods using delegate

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
{
	BaseDataCenter *request = [[[self class] alloc] initWithDelegate:delegate
                                                          parameters:nil
                                                 indicatorParentView:nil
                                                            cacheKey:nil
                                                           cacheType:DataCacheManagerCacheTypeMemory];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
           withParameters:(NSDictionary*)params
{
	BaseDataCenter *request = [[[self class] alloc] initWithDelegate:delegate
                                                          parameters:params
                                                 indicatorParentView:nil
                                                            cacheKey:nil
                                                           cacheType:DataCacheManagerCacheTypeMemory];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
               parameters:(NSDictionary*)params
                 cacheKey:(NSString*)cache
                cacheType:(DataCacheManagerCacheType)cacheType
{
    
	BaseDataCenter *request = [[[self class] alloc] initWithDelegate:delegate
                                                          parameters:params
                                                 indicatorParentView:nil
                                                            cacheKey:cache
                                                           cacheType:cacheType];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
      indicatorParentView:(UIView*)indiView
{
	BaseDataCenter *request = [[[self class] alloc] initWithDelegate:delegate
                                                          parameters:nil
                                                 indicatorParentView:indiView
                                                            cacheKey:nil
                                                           cacheType:DataCacheManagerCacheTypeMemory];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
               parameters:(NSDictionary*)params
      indicatorParentView:(UIView*)indiView
{
	BaseDataCenter *request = [[[self class] alloc] initWithDelegate:delegate
                                                          parameters:params
                                                 indicatorParentView:indiView
                                                            cacheKey:nil
                                                           cacheType:DataCacheManagerCacheTypeMemory];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

+ (id)requestWithDelegate:(id<DataRequestDelegate>)delegate
               parameters:(NSDictionary*)params
      indicatorParentView:(UIView*)indiView
                 cacheKey:(NSString*)cache
                cacheType:(DataCacheManagerCacheType)cacheType
{
	BaseDataCenter *request = [[[self class] alloc] initWithDelegate:delegate
                                                          parameters:params
                                                 indicatorParentView:indiView
                                                            cacheKey:cache
                                                           cacheType:DataCacheManagerCacheTypeMemory];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

- (id)initWithDelegate:(id<DataRequestDelegate>)delegate
            parameters:(NSDictionary*)params
   indicatorParentView:(UIView*)indiView
              cacheKey:(NSString*)cacheKey
             cacheType:(DataCacheManagerCacheType)cacheType
{
    
	return [self initWithDelegate:delegate
                       requestUrl:nil
                       parameters:params
              indicatorParentView:indiView
                   loadingMessage:nil
                         cacheKey:cacheKey
                        cacheType:cacheType];
}

- (id)initWithDelegate:(id<DataRequestDelegate>)delegate
            requestUrl:(NSString*)url
            parameters:(NSDictionary*)params
   indicatorParentView:(UIView*)indiView
        loadingMessage:(NSString*)msg
              cacheKey:(NSString*)cache
             cacheType:(DataCacheManagerCacheType)cacheType
{
	if(self = [super init])
    {
        self.requestUrl = url;
        if (!_requestUrl)
        {
            self.requestUrl = [self getRequestUrl];
        }
        
		self.indicatorView = indiView;
        self.loadingMessage = msg;
        
		self.delegate = delegate;
        
        self.cacheType = cacheType;
        self.cacheKey = cache;
        
        BOOL useCurrentCache = NO;
        NSObject *cacheData = [[DataCacheManager sharedManager] getCachedObjectByKey:cache];
        if (cacheData)
        {
            useCurrentCache = [self onReceivedCacheData:cacheData];
        }
        
        if (!useCurrentCache)
        {
            [self doRequestWithParams:params];
            NSLog(@"request %@ is created", [self class]);
        } else {
            [self performSelector:@selector(doRelease) withObject:nil afterDelay:0.1f];
        }
	}
	return self;
}

#pragma mark - init methods using blocks

+ (id)requestWithParameters:(NSDictionary*)params
          indicatorParentView:(UIView*)indiView
          onRequestFinished:(void(^)(BaseDataCenter *request))onFinishedBlock
{
    
	BaseDataCenter *request = [[[self class] alloc] initWithParameters:params
                                                            requestUrl:nil
                                                   indicatorParentView:indiView
                                                    loadingMessage:nil
                                                              cacheKey:nil
                                                             cacheType:DataCacheManagerCacheTypeMemory
                                                        onRequestStart:nil
                                                     onRequestFinished:onFinishedBlock
                                                     onRequestCanceled:nil
                                                       onRequestFailed:nil];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

+ (id)requestWithParameters:(NSDictionary*)params
        indicatorParentView:(UIView*)indiView
             onRequestStart:(void(^)(BaseDataCenter *request))onStartBlock
          onRequestFinished:(void(^)(BaseDataCenter *request))onFinishedBlock
          onRequestCanceled:(void(^)(BaseDataCenter *request))onCanceledBlock
            onRequestFailed:(void(^)(BaseDataCenter *request, NSError *error))onFailedBlock
{
	BaseDataCenter *request = [[[self class] alloc] initWithParameters:params
                                                            requestUrl:nil
                                                   indicatorParentView:indiView
                                                        loadingMessage:nil
                                                              cacheKey:nil
                                                             cacheType:DataCacheManagerCacheTypeMemory
                                                        onRequestStart:onStartBlock
                                                     onRequestFinished:onFinishedBlock
                                                     onRequestCanceled:onCanceledBlock
                                                       onRequestFailed:onFailedBlock];
    [[RequestManager sharedManager] addRequest:request];
    [request release];
    return request;
}

- (id)initWithParameters:(NSDictionary *)params
              requestUrl:(NSString *)url
     indicatorParentView:(UIView *)indiView
          loadingMessage:(NSString *)msg
                cacheKey:(NSString *)cache
               cacheType:(DataCacheManagerCacheType)cacheType
          onRequestStart:(void(^)(BaseDataCenter *request))onStartBlock
       onRequestFinished:(void(^)(BaseDataCenter *request))onFinishedBlock
       onRequestCanceled:(void(^)(BaseDataCenter *request))onCanceledBlock
         onRequestFailed:(void(^)(BaseDataCenter *request, NSError *error))onFailedBlock
{
	if(self = [super init])
    {
        self.requestUrl = url;
        if (!_requestUrl)
        {
            self.requestUrl = [self getRequestUrl];
        }
		self.indicatorView = indiView;
        self.loadingMessage = msg;
        
        _cacheType = cacheType;
        self.cacheKey = cache;
        
        if (onStartBlock)
        {
            self.onRequestStartBlock = onStartBlock;
        }
        if (onFinishedBlock)
        {
            self.onRequestFinishBlock = onFinishedBlock;
        }
        if (onCanceledBlock)
        {
            self.onRequestCanceled = onCanceledBlock;
        }
        if (onFailedBlock)
        {
            self.onRequestFailedBlock = onFailedBlock;
        }
        
        BOOL useCurrentCache = NO;
        NSObject *cacheData = [[DataCacheManager sharedManager] getCachedObjectByKey:cache];
        if (cacheData)
        {
            useCurrentCache = [self onReceivedCacheData:cacheData];
        }
        
        if (!useCurrentCache)
        {
            [self doRequestWithParams:params];
            NSLog(@"request %@ is created", [self class]);
        } else {
            [self performSelector:@selector(doRelease) withObject:nil afterDelay:0.1f];
        }
	}
	return self;
}

- (void)doRequestWithParams:(NSDictionary*)params
{
    if ([self getRequestMethod] == kRequestMethodGet)
    {
        [self startGetRequestWithUrl:_requestUrl parameters:params];
    } else {
        [self startPostRequestWithUrl:_requestUrl parameters:params];
    }
}

- (BOOL)handleResultString:(NSString*)resultString
{    
    NSString *trimmedString = [resultString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if (!trimmedString || [trimmedString length] == 0)
    {
		NSLog(@"!empty response error with Request:%@",[self class]);
		return NO;
	}
    
    // array or dictionary
    id resultData = [trimmedString objectFromJSONString];
	if(!resultData)
    {
        if (self.delegate)
        {
            // using delegate
            if ([self.delegate respondsToSelector:@selector(request:didFailLoadWithError:)])
            {
                [self.delegate request:self didFailLoadWithError:[NSError errorWithDomain:NSURLErrorDomain
                                                                                 code:0
                                                                             userInfo:nil]];
            }
        } else {
            // using block callback
            if (_onRequestFailedBlock)
            {
                _onRequestFailedBlock(self,[NSError errorWithDomain:NSURLErrorDomain
                                                               code:0
                                                           userInfo:nil]);
            }
        }
        return NO;
	} else {
        self.responseObject = resultData;
        [self processResult];
        
        if (self.result.code == 0 && _cacheKey)
        {
            if (_cacheType == DataCacheManagerCacheTypeMemory)
            {
                [[DataCacheManager sharedManager] addObjectToMemory:_responseObject forKey:_cacheKey];
            } else {
                [[DataCacheManager sharedManager] addObject:_responseObject forKey:_cacheKey];
            }
        }
        
        if (self.delegate)
        {
            if([self.delegate respondsToSelector:@selector(requestDidFinishLoad:)])
            {
                [self.delegate requestDidFinishLoad:self];
            }
        } else {
            if (_onRequestFinishBlock)
            {
                _onRequestFinishBlock(self);
            }
        }
        return YES;
    }
}

- (void)processResult
{
    _result = [[ResponseResult alloc] init];
}

- (void)cancelRequest
{
    self.request.delegate = nil;
    [self.request cancel];
    if (self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(requestDidCancelLoad:)])
        {
            [self.delegate requestDidCancelLoad:self];
        }
    } else {
        if (_onRequestCanceled)
        {
            _onRequestCanceled(self);
        }
    }
    self.delegate = nil;
    NSLog(@"%@ request is cancled!", [self class]);
}

- (BOOL)onReceivedCacheData:(id)cacheData
{
    // return yes to finish this request, return no to continue request from server
    NSLog(@"using cache data for request:%@", [self class]);
    
    if (cacheData && [cacheData isKindOfClass:[NSDictionary class]])
    {
        self.responseObject = [NSMutableDictionary dictionaryWithDictionary:cacheData];
        
        ResponseResult *cacheResult = [[ResponseResult alloc] initWithResult:[self.responseObject objectForKey:@"result"] code:[self.responseObject objectForKey:@"code"] message:[self.responseObject objectForKey:@"message"]];
        self.result = cacheResult;
        [cacheResult release];
        
        if (self.delegate)
        {
            if([self.delegate respondsToSelector:@selector(requestDidFinishLoad:)])
            {
                [self.delegate requestDidFinishLoad:self];
            }
        } else {
            if (_onRequestFinishBlock)
            {
                _onRequestFinishBlock(self);
            }
        }
        return YES;
    } else {
        NSLog(@"request:[%@],cache data should be handled by subclass", [self class]);
        return NO;
    }
}
#pragma mark waitting view show/hide
#define kHudTag 1135
- (void)showWaitView
{
    [self hideWaitView];
    
    if (_indicatorView)
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.indicatorView animated:YES];
        hud.removeFromSuperViewOnHide = YES;
        hud.tag = kHudTag;
        hud.labelText = self.loadingMessage;
    }
}

- (void)hideWaitView
{
    if (_indicatorView)
    {
        MBProgressHUD *hud = (MBProgressHUD *)[self.indicatorView viewWithTag:kHudTag];
        if (hud)
        {
            [hud hide:YES];
        }
    }
}

- (void)doRelease
{
    // remove self from Request Manager to release self;
    [[RequestManager sharedManager] removeRequest:self];
}

- (RequestMethod)getRequestMethod
{
	return kRequestMethodGet;
}

- (NSString*)getRequestUrl
{
	return @"";
}

- (NSString *)getErrorNameWithErrorCode:(NSInteger)code
{
    NSString *errorMsg = nil;
    switch (code)
    {
        case ASIConnectionFailureErrorType:
            errorMsg = @"无法连接到网络";
            break;
        case ASIRequestTimedOutErrorType:
            errorMsg = @"访问超时";
            break;
        case ASIAuthenticationErrorType:
            errorMsg = @"服务器身份验证失败";
            break;
        case ASIRequestCancelledErrorType:
            errorMsg = @"服务器请求已取消";
            break;
        case ASIUnableToCreateRequestErrorType:
            errorMsg = @"无法创建服务器请求";
            break;
        case ASIInternalErrorWhileBuildingRequestType:
            errorMsg = @"服务器请求创建异常";
            break;
        case ASIInternalErrorWhileApplyingCredentialsType:
            errorMsg = @"服务器请求异常";
            break;
        case ASIFileManagementError:
            errorMsg = @"服务器请求异常";
            break;
        case ASIUnhandledExceptionError:
            errorMsg = @"未知请求异常异常";
            break;
        default:
            errorMsg = @"服务器故障或网络链接失败！";
            break;
    }
    return errorMsg;
}

#pragma mark - request delegate methods

- (void)requestStarted:(ASIFormDataRequest*)request
{
    [self showWaitView];
    
    if (self.delegate)
    {
        if([self.delegate respondsToSelector:@selector(requestDidStartLoad:)])
        {
            [self.delegate requestDidStartLoad:self];
        }
    } else {
        if (self.onRequestStartBlock)
        {
            self.onRequestStartBlock(self);
        }
    }
}

- (void)requestFinished:(ASIFormDataRequest*)request
{
    [self hideWaitView];
    
	NSString *responseString = nil;
    if (request.allowCompressedResponse)
    {
        responseString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
    } else {
        responseString = [[NSString alloc] initWithData:[request rawResponseData] encoding:NSUTF8StringEncoding];
    }
    
	[self handleResultString:responseString];
    [responseString release];
    [self doRelease];
}

- (void)requestFailed:(ASIFormDataRequest*)request
{
    [self hideWaitView];
    
	NSLog(@"http request error:\n request:%@\n error:%@",[request.url absoluteString],request.error);
    
    if (self.delegate)
    {
        if ([self.delegate respondsToSelector:@selector(request:didFailLoadWithError:)])
        {
            [self.delegate request:self didFailLoadWithError:request.error];
        }
    } else {
        if (self.onRequestFailedBlock)
        {
            self.onRequestFailedBlock(self,request.error);
        }
    }
	
    [self doRelease];
}

@end
