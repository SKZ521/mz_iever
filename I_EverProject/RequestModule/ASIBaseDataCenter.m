//
//  ASIBaseDataCenter.m
//  ASIHttpRequestSummarize
//
//  Created by LLBT_wanggb on 14-5-27.
//  Copyright (c) 2014年 LLBT. All rights reserved.
//

#import "ASIBaseDataCenter.h"

// timeout
#define kRequestTimeOut 60

@interface ASIBaseDataCenter ()<ASIHTTPRequestDelegate>

@end

@implementation ASIBaseDataCenter

- (void)startGetRequestWithUrl:(NSString *)url parameters:(NSDictionary *)parameters
{
    if (!parameters) return;
    
    __block NSString *paramString = @"";
    [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSUInteger index = [[parameters allKeys] indexOfObject:key];
        paramString = [paramString stringByAppendingString:(index == 0)?@"":@"&"];
        paramString = [paramString stringByAppendingFormat:@"%@=%@",key,[self encodeURL:obj]];
    }];
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",url,paramString];
    NSURL *nsUrl = [NSURL URLWithString:urlStr];
    _request = [[ASIFormDataRequest alloc] initWithURL:nsUrl];
    
    [_request setRequestMethod:@"GET"];
    _request.delegate = self;
    _request.defaultResponseEncoding = NSUTF8StringEncoding;
    _request.timeOutSeconds = kRequestTimeOut;
    _request.allowCompressedResponse = YES;
    _request.shouldCompressRequestBody = NO;
    [_request startAsynchronous];
}

- (void)startPostRequestWithUrl:(NSString *)url parameters:(NSDictionary *)parameters
{
    if (!parameters) return;
    
    NSURL *nsUrl = [NSURL URLWithString:url];
    _request = [[ASIFormDataRequest alloc] initWithURL:nsUrl];
    [_request setRequestMethod:@"POST"];
    __block ASIPostFormat postFormat = ASIURLEncodedPostFormat;
    [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([obj isKindOfClass:[NSData class]])
        {
            postFormat = ASIMultipartFormDataPostFormat;
            [_request addData:obj withFileName:@"image.jpg" andContentType:@"image/jpeg" forKey:key];
        } else if ([obj isKindOfClass:[UIImage class]]) {
            postFormat = ASIMultipartFormDataPostFormat;
            NSData* data = UIImageJPEGRepresentation(obj, 0.8);
            [_request addData:data withFileName:@"image.jpg" andContentType:@"image/jpeg" forKey:key];
        } else {
            [_request addPostValue:obj forKey:key];
        }
    }];
    _request.postFormat = postFormat;
    _request.delegate = self;
    _request.defaultResponseEncoding = NSUTF8StringEncoding;
    _request.timeOutSeconds = kRequestTimeOut;
    _request.allowCompressedResponse = YES;
    _request.shouldCompressRequestBody = NO;
    [_request startAsynchronous];
}

#pragma mark - private
- (NSString*)encodeURL:(NSString *)string
{
	NSString *newString = [(NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)string, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)) autorelease];
	if (newString)
    {
		return newString;
	}
	return @"";
}

@end
