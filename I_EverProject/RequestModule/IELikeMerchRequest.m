//
//  IELikeMerchRequest.m
//  I_EverProject
//
//  Created by 王光宾 on 14-7-6.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import "IELikeMerchRequest.h"

@implementation IELikeMerchRequest

- (RequestMethod)getRequestMethod
{
    return kRequestMethodPost;
}

- (NSString *)getRequestUrl
{
    return [NSString stringWithFormat:@"http://218.244.148.190/api/Product.asmx/likeProduct"];
}

- (void)processResult
{
    [super processResult];
    NSLog(@"%s--商品点赞返回信息-----%@",__FUNCTION__,self.responseObject);
}

@end
