//
//  DataCacheManager.m
//  LLBTFramework
//
//  Created by li.zhenjie on 14-1-24.
//  Copyright (c) 2014年 chinamworld. All rights reserved.
//

#import "DataCacheManager.h"

#define RELEASE_SAFELY(__POINTER) { [__POINTER release]; __POINTER = nil;}

@interface DataCacheManager()

@end

@implementation DataCacheManager

LLBTOBJECT_SINGLETON_BOILERPLATE(DataCacheManager, sharedManager)

#pragma mark - lifecycle methods
- (void)dealloc
{
    RELEASE_SAFELY(_memoryCacheKeys);
    RELEASE_SAFELY(_memoryCachedObjects);
    RELEASE_SAFELY(_keys);
    RELEASE_SAFELY(_cachedObjects);
    [super dealloc];
}

- (id)init
{
    if(self = [super init])
    {
        [self registerMemoryWarningNotification];
    }
    return self;
}

#pragma mark - private methods
- (void)registerMemoryWarningNotification
{
#if TARGET_OS_IPHONE
    // Subscribe to app events
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(clearMemoryCache)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification
                                               object:nil];
#ifdef __IPHONE_4_0
    UIDevice *device = [UIDevice currentDevice];
    if ([device respondsToSelector:@selector(isMultitaskingSupported)] && device.multitaskingSupported){
        // When in background, clean memory in order to have less chance to be killed
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(clearMemoryCache)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
#endif
#endif
}

- (void)restore
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_DATA_CACHE_KEYS]) {
        NSArray *keysArray = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_DATA_CACHE_KEYS]];
        _keys = [[NSMutableArray alloc] initWithArray:keysArray];
    }else{
        _keys = [[NSMutableArray alloc] init];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_DATA_CACHE_OBJECTS]){
        NSDictionary *objDic = (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_DATA_CACHE_OBJECTS]];
        _cachedObjects = [[NSMutableDictionary alloc] initWithDictionary:objDic];
    }else{
        _cachedObjects = [[NSMutableDictionary alloc] init];
    }
    _memoryCacheKeys = [[NSMutableArray alloc] init];
    _memoryCachedObjects = [[NSMutableDictionary alloc] init];
}

- (BOOL)isValidKey:(NSString*)key
{
    if (!key || [key length] == 0 || (NSNull*)key == [NSNull null]) {
        return NO;
    }
    return YES;
}

- (void)removeKey:(NSString *)key fromKeyArray:(NSMutableArray *)keyArray
{
    if ([keyArray containsObject:key])
    {
        [keyArray removeObject:key];
    }
}

#pragma mark - public methods

- (void)doSave
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:_keys] forKey:UD_KEY_DATA_CACHE_KEYS];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:_cachedObjects] forKey:UD_KEY_DATA_CACHE_OBJECTS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)clearAllCache
{
    [self clearMemoryCache];
    [_keys removeAllObjects];
    [_cachedObjects removeAllObjects];
    [self doSave];
}

- (void)clearMemoryCache
{
    [_memoryCacheKeys removeAllObjects];
    [_memoryCachedObjects removeAllObjects];
}

- (void)addObject:(NSObject*)obj forKey:(NSString*)key
{
    if (![self isValidKey:key]) {
        return;
    }
    if (!obj || (NSNull*)obj == [NSNull null]) {
        return;
    }
    if ([self hasObjectInCacheByKey:key])
    {
        [self removeObjectInCacheByKey:key];
    }
    [_keys addObject:key];
    _cachedObjects[key] = obj;
    [self doSave];
}

- (void)addObjectToMemory:(NSObject*)obj forKey:(NSString*)key
{
    if (![self isValidKey:key])
    {
        return;
    }
    if (!obj || (NSNull*)obj == [NSNull null])
    {
        return;
    }
    if ([self hasObjectInCacheByKey:key])
    {
        [self removeObjectInCacheByKey:key];
    }
    [_memoryCacheKeys addObject:key];
    _memoryCachedObjects[key] = obj;
}

- (NSObject*)getCachedObjectByKey:(NSString*)key
{
    if (![self isValidKey:key])
    {
        return nil;
    }
    if (_memoryCachedObjects[key])
    {
        return _memoryCachedObjects[key];
    } else {
        return _cachedObjects[key];
    }
}

- (BOOL)hasObjectInCacheByKey:(NSString*)key
{
    return [self getCachedObjectByKey:key] != nil;
}

- (void)removeObjectInCacheByKey:(NSString*)key
{
    if (![self isValidKey:key])
    {
        return;
    }
    [_cachedObjects removeObjectForKey:key];
    [self removeKey:key fromKeyArray:_keys];
    [_memoryCachedObjects removeObjectForKey:key];
    [self removeKey:key fromKeyArray:_memoryCacheKeys];
}

@end
