//
//  DataCacheManager.h
//  LLBTFramework
//  轻量级缓存管理器，利用UserDefault进行缓存管理，大数据量是效率有问
//
//  Created by li.zhenjie on 14-1-24.
//  Copyright (c) 2014年 chinamworld. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UD_KEY_DATA_CACHE_KEYS @"UD_KEY_DATA_CACHE_KEYS"
#define UD_KEY_DATA_CACHE_OBJECTS @"UD_KEY_DATA_CACHE_OBJECTS"

#define DATA_CACHE_KEY_ALL_TOPIC_LIST @"DATA_CACHE_KEY_ALL_TOPIC_LIST"
#define DATA_CACHE_KEY_MY_TOPIC_LIST @"DATA_CACHE_KEY_MY_TOPIC_LIST"
#define DATA_CACHE_KEY_UPDATED_TOPIC_LIST @"DATA_CACHE_KEY_UPDATED_TOPIC_LIST"

typedef NS_ENUM(NSUInteger, DataCacheManagerCacheType)
{
    DataCacheManagerCacheTypeMemory,
	DataCacheManagerCacheTypePersist
};

@interface DataCacheManager : NSObject
{
    NSMutableArray *_memoryCacheKeys;     // keys for objects only cached in memory
    NSMutableDictionary *_memoryCachedObjects;     // objects only cached in memory
    NSMutableArray *_keys;          // keys for keys not managed by queue
    NSMutableDictionary *_cachedObjects;
}

+ (DataCacheManager *)sharedManager;

- (void)addObject:(NSObject*)obj forKey:(NSString*)key;
- (void)removeObjectInCacheByKey:(NSString*)key;
- (NSObject*)getCachedObjectByKey:(NSString*)key;
- (BOOL)hasObjectInCacheByKey:(NSString*)key;
- (void)clearAllCache;

- (void)addObjectToMemory:(NSObject*)obj forKey:(NSString*)key;
- (void)clearMemoryCache;

- (void)doSave;

@end
