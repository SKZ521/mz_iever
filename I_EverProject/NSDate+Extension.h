//
//  NSDate+Extension.h
//  I_EverProject
//
//  Created by LLBT_wanggb on 14-7-8.
//  Copyright (c) 2014年 i_ever. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Convenience)

- (NSInteger)year;
- (NSInteger)month;
- (NSInteger)week;
- (NSInteger)day;

-(NSDate *)offsetMonth:(int)numMonths;
-(NSDate *)offsetDay:(int)numDays;
-(NSDate *)offsetHours:(int)hours;
-(int)numDaysInMonth;
-(int)firstWeekDayInMonth;

+(NSDate *)dateStartOfDay:(NSDate *)date;
+(NSDate *)dateStartOfWeek;
+(NSDate *)dateEndOfWeek;

@end
